--
-- Populates the wikipedia_preview_stats table daily,
-- filtering webrequest data and aggregating it into interesting dimensions.
--
-- Parameters:
--     source_table         -- Fully qualified table name of the source webrequest data.
--     destination_table    -- Fully qualified table name for the wikipedia preview stats.
--     year                 -- Year of the partition to process.
--     month                -- Month of the partition to process.
--     day                  -- Day of the partition to process.
--
-- Usage:
--     spark3-sql -f generate_wikipedia_preview_stats_daily.hql \
--         -d source_table=wmf.webrequest \
--         -d destination_table=wmf_product.wikipedia_preview_stats \
--         -d year=2023 \
--         -d month=5 \
--         -d day=21
--


INSERT OVERWRITE TABLE ${destination_table}
    PARTITION(year=${year}, month=${month}, day=${day})
SELECT /*+ COALESCE(1) */
    SUM(CAST(is_pageview AS INT)) AS pageviews,
    SUM(CAST(NOT is_pageview AS INT)) AS previews,
    IF(
        REGEXP_EXTRACT(
            x_analytics_map['wprov'],
            '^wppw(\\d+)(t?)$',
            2
        ) = 't',
        IF(
            -- "Mac OS X" touch devices are iPads
            user_agent_map['os_family'] IN ('Android', 'iOS', 'Mac OS X'),
            'touch',
            'touchscreen computer'
        ),
        'non-touch'
    ) AS device_type,
    parse_url(referer, 'HOST') AS referer_host,
    geocoded_data['continent'] AS continent,
    geocoded_data['country_code'] AS country_code,
    geocoded_data['country'] AS country,
    CAST(REGEXP_EXTRACT(
        x_analytics_map['wprov'],
        '^wppw(\\d+)(t?)$',
        1
    ) AS INT) AS instrumentation_version
FROM
    ${source_table}
WHERE
    x_analytics_map['wprov'] REGEXP '^wppw(\\d+)(t?)$'
    AND agent_type = 'user'
    AND webrequest_source = 'text'
    AND year = ${year}
    AND month = ${month}
    AND day = ${day}
GROUP BY
    IF(
        REGEXP_EXTRACT(
            x_analytics_map['wprov'],
            '^wppw(\\d+)(t?)$',
            2
        ) = 't',
        IF(
            user_agent_map['os_family'] IN ('Android', 'iOS', 'Mac OS X'),
            'touch',
            'touchscreen computer'
        ),
        'non-touch'
    ),
    parse_url(referer, 'HOST'),
    geocoded_data['continent'],
    geocoded_data['country_code'],
    geocoded_data['country'],
    CAST(REGEXP_EXTRACT(
        x_analytics_map['wprov'],
        '^wppw(\\d+)(t?)$',
        1
    ) AS INT)
;
