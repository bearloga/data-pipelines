-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregation for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     coalesce_partitions  -- Number of partitions to write
--     target_year          -- Year of partition to compute aggregation
--                             for.
--     target_month         -- Month of partition to compute aggregation
--                             for.
--     target_day           -- Day of partition to compute aggregation
--                             for.
--
-- spark3-sql -f citation_needed_searches_daily.hql                            \
--            -d source_table=event.mediawiki_cirrussearch_request             \
--            -d destination_table=wmf_product.citation_needed_searches_daily  \
--            -d coalesce_partitions=1                                         \
--            -d target_year=2024                                              \
--            -d target_month=3                                                \
--            -d target_day=1

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    day = TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    )
;

-- Compute data for the period
INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
    `database` AS wiki_db,
    COUNT(1) AS search_count,
    TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    ) AS day
FROM
    ${source_table}
WHERE
    http.request_headers['user-agent'] = 'citation-needed-api'
    AND year = ${target_year}
    AND month = ${target_month}
    AND day = ${target_day}
GROUP BY `database`
