-- Create table statement for citation_needed_clickthroughs_daily table.
--
-- Parameters:
--     destination_table    -- Name of the table to create
--                             (database name provided through spark3-sql
--                              command-line argument).
--     location             -- HDFS folder path to place the table files in.
--
-- Usage
--     spark3-sql -f create_citation_needed_clickthroughs_daily_table.hql   \
--     --database wmf_product                                               \
--     -d destination_table=citation_needed_clickthroughs_daily             \
--     -d location=/wmf/data/wmf_product/citation_needed_clickthroughs_daily

CREATE EXTERNAL TABLE IF NOT EXISTS `${destination_table}`(
    `wikipedia_lang`  string  COMMENT 'Wikipedia project language code (e.g. en)',
    `view_count`      bigint  COMMENT 'Number of pageviews that were clicks from Citation Needed extension',
    `day`             date    COMMENT 'The day for which the metric is computed over.'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${location}'
;
