-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregation for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     coalesce_partitions  -- Number of partitions to write
--     target_year          -- Year of partition to compute aggregation
--                             for.
--     target_month         -- Month of partition to compute aggregation
--                             for.
--     target_day           -- Day of partition to compute aggregation
--                             for.
--
-- spark3-sql -f citation_needed_clickthroughs_daily.hql                           \
--            --master yarn --conf spark.dynamicAllocation.maxExecutors=64         \
--            --executor-memory 8G --executor-cores 4 --driver-memory 2G           \
--            -d source_table=wmf.pageview_actor                                   \
--            -d destination_table=wmf_product.citation_needed_clickthroughs_daily \
--            -d coalesce_partitions=1                                             \
--            -d target_year=2024                                                  \
--            -d target_month=3                                                    \
--            -d target_day=21

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    day = TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    )
;

-- Compute data for the period
INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
    normalized_host.project AS wikipedia_lang,
    COUNT(1) AS view_count,
    TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    ) AS day
FROM ${source_table}
WHERE
    year = ${target_year}
    AND month = ${target_month}
    AND day = ${target_day}
    AND x_analytics_map['wprov'] = 'cna1'
    AND agent_type = 'user'
GROUP BY normalized_host.project;
