# Data Pipelines

Hive queries (and eventually Jupyter notebooks) for [Product Analytics](https://www.mediawiki.org/wiki/Product_Analytics) team's data pipelines.

The corresponding Airflow DAGs are located separately in [repos/data-engineering/airflow-dags/analytics_product/dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/analytics_product/dags)

## Development

Please no significant development directly in main branch. When adding a new set of queries, please create them in a separate branch or a fork. When ready, open a merge request to bring your changes into the main branch. This allows for maintainers of this repository to conduct a code review and provide feedback.

For minor changes (e.g. fixing typos that didn't get caught in code review) it's okay to work directly on main.

All queries which populate a table require a corresponding query which creates the table.

### Testing

Please test your queries prior to opening a merge request.

**Prerequisite**: create a personal database in Hive if one does not exist already

To test:

First, run the table creation query by using the command in the usage section, replacing the database name with the name of your personal database and the base directory with a temporary one:

For example, if testing [create_wikipedia_chatgpt_plugin_searches_table.hql](wikipedia_chatgpt_plugin/searches/create_wikipedia_chatgpt_plugin_searches_table.hql):

```diff
spark3-sql -f create_wikipedia_chatgpt_plugin_searches_table.hql \
-          -d table_name=wmf_product.wikipedia_chatgpt_plugin_searches \
+          -d table_name=bearloga.wikipedia_chatgpt_plugin_searches \
-          -d base_directory=/user/analytics-product/data/wikipedia_chatgpt_plugin_searches/daily
+          -d base_directory=/tmp/bearloga/wikipedia_chatgpt_plugin_searches_daily
```

Change the parameters of the table-populating query accordingly:

```diff
spark3-sql -f generate_wikipedia_chatgpt_plugin_searches_daily.hql \
           -d source_table=event.mediawiki_cirrussearch_request \
-          -d destination_table=wmf_product.wikipedia_chatgpt_plugin_searches \
+          -d destination_table=bearloga.wikipedia_chatgpt_plugin_searches \
           -d year=2023 \
           -d month=7 \
           -d day=11
```

## Deployment

The corresponding Airflow DAG's `hqi_uri` parameter needs an immutable URI for raw contents of the query.

The quickest way to retrieve the URI is:
1. Navigate to the .hql file in GitLab
2. Click **Permalink** button, which will update your browser's address bar
3. Do not click **Open raw** button yet, because it's still pointing at the mutable "latest" version. Instead, refresh the page.
4. After refreshing the page, clicking the **Open raw** button will correctly open the query at a specific commit SHA

The URI should look like: `https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/40fcc4126c1029a94992cef3c74600ef92741ec8/wikipedia_chatgpt_plugin/searches/generate_wikipedia_chatgpt_plugin_searches_daily.hql`

Notice the 40-character alphanumeric commit SHA in the URI. If you see `/raw/main/` in the URI, try the steps again.

**Example**: [wikipedia_chatgpt_plugin_searches_daily_dag.py](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics_product/dags/wikipedia_chatgpt_plugin/wikipedia_chatgpt_plugin_searches_daily_dag.py)

